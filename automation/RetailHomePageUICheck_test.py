import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By

defaultURL = "https://www.fetchtv.com.au/activate"

class RetailHomePageUITest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        #create a new Firefox session
        cls.driver = webdriver.Firefox()
        cls.driver.implicitly_wait(10)
        cls.driver.maximize_window()
        cls.driver.get(defaultURL)

    def test_fetch_logo(self):
        # check if fetch logo is exist
        print ("Checking Fetch Logo ...")
        self.assertTrue(self.is_element_present(By.XPATH,"//IMG[@src='//www.fetchtv.com.au/img/fetchtv-logo.png']"))

    def test_feature_menu(self):
        print ("Checking Feature Menu ...")
        self.assertTrue(self.is_element_present(By.XPATH,"//SPAN[text()='Features']"))

    def test_tv_menu(self):
        print ("Checking TV Menu ...")
        self.assertTrue(self.is_element_present(By.XPATH,"//SPAN[text()='TV']"))

    def test_movies_menu(self):
        print ("Checking Movies Menu ...")
        self.assertTrue(self.is_element_present(By.XPATH,"//SPAN[text()='Movies']"))

    def test_packages_menu(self):
        print ("Checking Packages Menu ...")
        self.assertTrue((self.is_element_present(By.XPATH,"//SPAN[text()='Packages']")))

    def test_account_menu(self):
        print ("Checking Account Menu ...")
        self.assertTrue((self.is_element_present(By.XPATH,"//SPAN[@class='account']")))

    def test_h1(self):
        # Activate Fetch TV
        print ("Checking 'Activate Fetch TV' text ...")
        self.assertTrue((self.is_element_present(By.XPATH,"//H1[@class='top-container']")))

    def test_h2(self):
        # Congratulations on the purchase of your new Fetch TV box.
        print ("Checking 'Congratulations on the purchase of your new Fetch TV box.' text ...")
        self.assertTrue((self.is_element_present(By.XPATH,"//H2[@class='to-pink'][text()='Congratulations on the purchase of your new Fetch TV box.']")))

    def test_h3(self):
        # Next create an account and get your activation code
        print ("Checking 'Next create an account and get your activation code' text ...")
        self.assertTrue((self.is_element_present(By.XPATH,"//H3[text()='Next create an account and get your activation code']")))

    def test_button(self):
        # Get Started
        print ("Checking the 'Get Started' button ...")
        self.assertTrue((self.is_element_present(By.XPATH,"//A[@id='getStarted']")))

    def test_h4(self):
        # Important user information
        print ("Checking 'Important user information' text ...")
        self.assertTrue((self.is_element_present(By.XPATH,"//H3[text()='Important user information']")))

    def test_h5(self):
        # Using the Fetch TV service requires
        print ("Checking 'Using the Fetch TV service requires' text ...")
        self.assertTrue((self.is_element_present(By.XPATH,"//H4[@class='to-grey'][text()='Using the Fetch TV service requires']")))

    def test_check_1(self):
        # Broadband Internet connection*
        print ("Checking 'Broadband Internet connection*' text ...")
        self.assertTrue((self.is_element_present(By.XPATH,"//DIV[@class='requirement'][text()='Broadband Internet connection*']")))

    def test_check_2(self):
        # HDMI connection on your TV
        print ("Checking 'HDMI connection on your TV' text ...")
        self.assertTrue((self.is_element_present(By.XPATH,"//DIV[@class='requirement'][text()='HDMI connection on your TV']")))

    def test_check_3(self):
        # Free to Air TV antenna
        print ("Checking 'Free to Air TV antenna' text ...")
        self.assertTrue((self.is_element_present(By.XPATH,"//DIV[@class='requirement'][text()='Free to Air TV antenna']")))

    def test_check_4(self):
        # Payment of a ... one-off activation fee payable by Visa or Mastercard
        print ("Checking 'Payment of a ... one-off activation fee payable by Visa or Mastercard' text ...")
        self.assertTrue(self.is_element_present(By.XPATH,"(//DIV[@class='requirement'])[4]"))

    def test_stb_image(self):
        print ("Checking STB image ...")
        self.assertTrue(self.is_element_present(By.XPATH,"//IMG[@src='img/fetch-stb.jpg']"))

    def test_term_condition_star(self):
        # *Use of the Fetch TV Box and service ...
        print ("Checking '*Use of the Fetch TV Box and service ..' text ...")
        self.assertTrue((self.is_element_present(By.XPATH,"(//SMALL)[3]")))

    def test_term_condition_plus(self):
        # Includes 3 movie rental credits ...
        print ("Checking 'Includes 3 movie rental credits ..' text ... ")
        self.assertTrue(self.is_element_present(By.XPATH,"(//SMALL)[4]"))

    def test_h6(self):
        # Still need a box?
        print ("Checking 'Still need a box?' text ...")
        self.assertTrue(self.is_element_present(By.XPATH,"//H3[@class='to-grey'][text()='Still need a box?']"))

    def test_h7(self):
        # Please visit www.fetchtv.com.au / packages
        print ("Checking 'Please visit www.fetchtv.com.au / packages' text ...")
        self.assertTrue(self.is_element_present(By.XPATH,"//A[@href='//www.fetchtv.com.au/packages'][text()='www.fetchtv.com.au/packages']"))

    def test_footer_section(self):
        print ("Checking footer section ...")
        self.assertTrue(self.is_element_present(By.XPATH,"//A[@href='//www.fetchtv.com.au/packages'][text()='www.fetchtv.com.au/packages']"))

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NotImplemented: return False
        return True

if __name__ == '__main__':
    unittest.main(verbosity=2)
    print ("\033[1m" + "----- Executing: Retail Home Page UI Check test case -----" + "\033[0m")
