import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException


activationArray = ["00000009","ub6d3n97gb","gx3grvsfgn"]
pinArray = ["0000", "0000", "0000"]
accountDetailsArray = ["Optus Active Standalone", "Optus Suspended Non Standalone",
                               "Active Retail Non Standalone"]
defaultURL = "https://www.fetchtv.com.au/account"

class SignInWithActivationCode(unittest.TestCase):
    @classmethod
    def setUp(inst):
        # create a new Firefox session
        inst.driver = webdriver.Firefox()
        inst.driver.implicitly_wait(30)
        inst.driver.maximize_window()
        inst.driver.delete_all_cookies()
        inst.driver.get(defaultURL)
        time.sleep(10)

    def test_sign_in_with_actiation_code(self):
        print ("\033[1m" + "----- Executing: Sign In With Activation Code test case -----" + "\033[0m")
        print ("#1. Open Firefox and go to " + defaultURL)
        for i in range(len(activationArray)):
            try:
                activationBox = self.driver.find_element_by_tag_name("input")
                signInButton = self.driver.find_element_by_class_name("btn")
                if (activationBox.is_displayed() and signInButton.is_displayed()):
                    print ("#2. Sign In with the Activation Code & Check if the Account is Active")
                    print ("[Account Details is " + accountDetailsArray[i] + " ]")
                    print ("[Activation Code is " + activationArray[i] + " ]")
                    print ("[User Defined PIN is " + pinArray[i] + " ]")
                    activationBox.clear()
                    time.sleep(5)
                    activationBox.send_keys(activationArray[i])
                    time.sleep(5)
                    signInButton.click()
                    self.driver.implicitly_wait(5)
                    try:
                        # check if the alert suspended message is displayed
                        if (self.driver.find_element_by_xpath(
                                "//DIV[@ng-show='logoutReason']//STRONG[@class='ng-binding'][text()='Activate your box before you sign in.']").is_displayed()):
                            print ("Confirm: Account is Suspended.")
                            print ("")
                            print ("End of Test Case")
                            print ("")
                    except NoSuchElementException:
                        print ("Confirm: Account is Active.")
                        self.userDefinedPIN(pinArray[i])
                else:
                    print ("Activation Box element is not displayed")
            except ValueError:
                print "Error in signInWithActivationCode"

    @classmethod
    def tearDown(inst):
        inst.driver.quit()

    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException:
            return False
        return True

    def userDefinedPIN(self, userDefinedPIN):
        print ("#3. Check User Defined PIN Flag")
        self.driver.implicitly_wait(10)
        pinInput = self.driver.find_element_by_tag_name("input")
        try:
            if (self.driver.find_element_by_xpath(
                    "//BUTTON[@class='btn btn-default']//SPAN[@class='ng-binding'][text()='Set PIN']").is_displayed()):
                print ("Confirm: User defined PIN is False")
                print ("#4. Set a new User Defined PIN")
                pinInput.send_keys(userDefinedPIN)
                self.driver.implicitly_wait(5)
                # check if the Confirm PIN button exist then re-confirm the new PIN
                if (self.driver.find_element_by_xpath(
                        "//button[contains(., 'Confirm PIN')]").is_displayed() is not None):
                    print ("#5. Re-confirm New User Defined PIN")
                    pinInput.send_keys(userDefinedPIN)
                    time.sleep(5)
                    print ("#6. Check If it's a Standlone Account by its landing page")
                    self.checkIfStandalone()
                    time.sleep(5)
                    print ("#7. Check the ISP Branding")
                    self.checkISPBranding()
                    time.sleep(5)
                    self.weNeedToLogout()
                else:
                    print "Failed to confirm User Defined PIN"
        except NoSuchElementException:
            print ("Confirm: User defined PIN is True")
            print ("#4. Enter existing User Defined PIN")
            pinInput.send_keys(userDefinedPIN)
            time.sleep(5)
            print ("#5. Check If it's a Standlone Account by its landing page")
            self.checkIfStandalone()
            time.sleep(5)
            print ("#6. Check the ISP Branding")
            self.checkISPBranding()
            time.sleep(5)
            self.weNeedToLogout()

    def checkIfStandalone(self):
        try:
            if (self.driver.find_element_by_xpath(
                    "//H2[@class='submenuHeading ng-binding'][text()='Wishlist']").is_displayed() is not None):
                print ("It's landing on Wishlist. It's a Standalone !")
        except NoSuchElementException:
            print ("It's landing on Settings. It's NOT a Standalone !")
        time.sleep(5)

    def checkISPBranding(self):
        try:
            if (self.driver.find_element_by_xpath(
                    "//div//h3[contains(., 'Yes TV by Fetch')]").is_displayed() is not None):
                print ("It's Optus !")
        except NoSuchElementException:
            print ("It's not Optus")

    def weNeedToLogout(self):
        try:
            if (self.driver.find_element_by_xpath("//SPAN[text()='Sign Out']").is_displayed() is not None):
                print ("We need to logout")
                self.driver.find_element_by_xpath("//SPAN[text()='Sign Out']").click()
                time.sleep(10)
                print ("Deleting all cookies ...")
                self.driver.delete_all_cookies()
                self.driver.implicitly_wait(10)
                print ("Going back to default URL ...")
                self.driver.get(defaultURL) #re-setting back to a default URL
                time.sleep(5)
                print ("")
                print ("End of Test Case")
                print ("")
        except NoSuchElementException:
            print ("We don't need to logout")
            print ("Deleting all cookies ...")
            self.driver.delete_all_cookies()
            self.driver.implicitly_wait(10)
            print ("Going back to default URL ...")
            self.driver.get(defaultURL)  #re-setting back to a default URL
            time.sleep(5)
            print ("")
            print ("End of Test Case")
            print ("")

if __name__ == '__main__':
    unittest.main()

'''
    def runningTest1(self):


        print ("\033[1m" + "----- Executing Test Case #1 -----" + "\033[0m")
        print ("#1. Open Firefox and go to " + url)

        for i in range(len(activationArray)):
            print ("\033[1m")
            print i + 1
            print ("[Account Details is " + accountDetailsArray[i] + " ]")
            print ("[Activation Code is " + activationArray[i] + " ]")
            print ("[User Defined PIN is " + pinArray[i] + " ]")
            print ("\033[0m")
            # browser_init.getUrl(url)
            functions.signInWithActivationCode(activationArray[i], pinArray[i])
            time.sleep(10)
            print ("")
            print ("End of Test Case")
            print ("")
            time.sleep(10)

'''

