import unittest
import HTMLTestRunner
import os
from SignInWithActivationCode_test import SignInWithActivationCode
from RetailHomePageUICheck_test import RetailHomePageUITest


# get all tests from classes
sign_in_test = unittest.TestLoader().loadTestsFromTestCase(SignInWithActivationCode)
retail_home_ui_test = unittest.TestLoader().loadTestsFromTestCase(RetailHomePageUITest)

# create a test suite
test_suite = unittest.TestSuite([sign_in_test,retail_home_ui_test])

# run the suite
unittest.TextTestRunner(verbosity=2).run(test_suite)

# open the report file
outfile = open("SeleniumPythonTestSummary.html", "w")


# configure HTMLTestRunner options
runner = HTMLTestRunner.HTMLTestRunner(stream=outfile, title='Test Report', description='Acceptance Tests')

# run the suite using HTMLTestRunner
runner.run(test_suite)